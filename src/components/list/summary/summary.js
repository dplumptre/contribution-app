import React from 'react';
import MaterialTable from 'material-table';
import ChevronLeft from '@material-ui/icons/ChevronLeft'
import Modal from '../../../Partials/Modal/Modal';
import PopupEditForm from'../../popupform/popupEditForm';
import Aux from '../../../Hoc/Aux/Aux';
import {connect} from 'react-redux';


const summary = (props) => {


  // useEffect(()=>{
  //   //console.log(11);
  //   props.falsifyRedirect();

  // });




  // save data popup form
let editData = null;
if(props.showEditSavings){
  editData  = (
    <Modal show={props.showEditSavings}>
    <div className=" mt-3">
    <PopupEditForm upstate={props.upstate} cancel={props.cancel} name={props.name} transaction={props.transaction} myprops={props.myprops} />
  </div>
  </Modal>
  )
}


    return ( 
      <Aux>
      <div className={["card","my-5"].join(' ')}>
      <div className="card-body">
      <h5 className="card-title text-center text-primary"> {props.name}'s summary account</h5>
        <MaterialTable
          columns={[

            { title: 'Date', field: 'mydate',type: 'date',
            render: rowData =>{
              let name = (<div className=" text-dark font-weight-bold">{rowData.mydate} </div>);
              if(rowData.status === "completed"){
                name = (<div className="text-secondary font-italic">{rowData.mydate}</div>);
              }
              return name;
            }, 
        },
 
        { title: 'Amount (₦)', field: 'amount',
        render: rowData =>{
          const num =  new Intl.NumberFormat().format(rowData.amount) 
          let name = (<div className=" text-dark font-weight-bold">{"₦ "+num} </div>);
          if(rowData.status === "completed"){
            name = (<div className="text-secondary font-italic">{"₦ "+num}</div>);
          }
          return name;
        }, 
       },

          ]}
          data={props.transactions}
          title="Summary"
          options={{
            actionsColumnIndex: -1
          }}
          actions={[
            {
              icon: ChevronLeft,
              tooltip: 'Go Back',
              isFreeAction: true,
              onClick: () => props.goBk()
            },
            {
              icon:'edit',
              tooltip:'edit',
             // isFreeAction:true,
              onClick: (event,rowData) => props.edit(rowData)
            },
            {
              icon:'delete',
              tooltip: 'Delete ',
              onClick: (event,rowData) => props.delete(rowData.id,rowData.amount,props.token)
            },
          ]}
        />
        </div>
        </div>
        {editData}
       </Aux>


       );
}


const mapStateToProps = state=>{
  return{
      token:state.authReducer.token,
      
  }
}

 
export default connect(mapStateToProps,null)(summary);