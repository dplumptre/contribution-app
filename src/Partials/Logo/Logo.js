import React from "react";

const logo = (props) => (
<img className={props.classes} src={props.name} alt=""/>
);

export default logo;