import * as actionTypes from '../actions/actionTypes'; 

const initialState ={
    userId: null,
    token : null,
    redirection:false,
    loading:false,
    success:null

}

const authReducer = (state = initialState, action)=>{
    switch(action.type){
        case actionTypes.AUTH_LOADING:
        return{
            ...state,
            redirection:false,
            loading:true
        }
        case actionTypes.AUTH_SUCCESS:
        return {
            ...state,
            loading:false,
            redirection:true,
            token:action.token,
            userId:action.userId
        }
        case actionTypes.AUTH_FALSIFY_REDIRECT:
        return{
            ...state,
            redirection:false
        }
        case actionTypes.AUTH_FAIL:
        return{
            ...state,
            loading:false,
            error: action.error
        }
        case actionTypes.AUTO_LOGOUT:
        return{
            ...state,
            token:null,
            userId:null
        } 
        case actionTypes.PASS_LOADING:
        return {
            ...state,
            loading: true
        }
        case actionTypes.PASS_SUCCESS:
        return {
            ...state,
            loading:false,
            success:action.success
        }
        case actionTypes.PASS_FAIL:
        return{
            ...state,
            loading:false,

        }

        default:
        return state
    }
}


export default authReducer;